resource "aws_security_group" "security_group_vpc_1" {
  name        = var.name
  vpc_id      = aws_vpc.vpc.id
  description = "default VPC security group"

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}