#OUTPUT VALUES FOR CALLING INTO MODULES
output "vpc_id" {
  description = "The CIDR block of the VPC"
  value       = aws_vpc.vpc.id
}

# output "public_subnet_id" {
#   value = aws_subnet.public_subnet.id
# }

# output "security_group_id" {
#   description = "id of security group"
#   value       = aws_security_group.security_group.id
# }