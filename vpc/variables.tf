variable "vpc_cidr_block" {
  description = "The CIDR block for the VPC"
  type        = string
}

variable "vpc_name" {
  description = "The name of the VPC"
  type        = string
}

variable "public_subnet1_name" {
  description = "The name of the public subnet"
  type        = string
}

variable "public_subnet2_name" {
  description = "The name of the public subnet"
  type        = string
}

variable "public_subnet1_cidr_block" {
  description = "The CIDR block for the public subnet "
  type        = string
}

variable "public_subnet2_cidr_block" {
  description = "The CIDR block for the public subnet "
  type        = string
}

variable "private_subnet_cidr1_block" {
  description = "The CIDR block for the public subnet "
  type        = string
}

variable "private_subnet1_name" {
  description = "The CIDR block for the public subnet "
  type        = string
}

variable "private_subnet_cidr2_block" {
  description = "The CIDR block for the public subnet "
  type        = string
}

variable "private_subnet2_name" {
  description = "The CIDR block for the public subnet "
  type        = string
}

variable "name" {
  description = "The name prefix of security groups"
  type        = string
}

# variable "internet_gateway_name" {
#   description = "The availablity zone for the public subnet 2 "
#   type        = string
# }

# variable "aws_region" {
#   description = "The AWS region where resources will be created"
#   type        = string
# }

# variable "public_subnet_cidr_block" {
#   description = "The CIDR block for the public subnet"
#   type        = string
# }

# variable "private_subnet_cidr_block" {
#   description = "The CIDR block for the private subnet"
#   type        = string
# }

# variable "public_sg_ingress_cidr_blocks" {
#   description = "A list of CIDR blocks to allow incoming traffic from for the public security group"
#   type        = list(string)
# }
