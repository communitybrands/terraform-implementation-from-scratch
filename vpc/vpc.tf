resource "aws_vpc" "vpc" {
  cidr_block = var.vpc_cidr_block

  tags = {
    Name = var.vpc_name
  }
}

resource "aws_subnet" "public_subnet_1" {
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = var.public_subnet1_cidr_block
  map_public_ip_on_launch = false
  tags = {
    Name = var.public_subnet1_name
  }
}
resource "aws_subnet" "public_subnet_2" {
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = var.public_subnet2_cidr_block
  # availability_zone       = var.availability_zone_public2
  map_public_ip_on_launch = false
  tags = {
    Name = var.public_subnet2_name
  }
}

resource "aws_subnet" "private_subnet1" {
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = var.private_subnet_cidr1_block
  map_public_ip_on_launch = false
  tags = {
    Name = var.private_subnet1_name
  }
}

resource "aws_subnet" "private_subnet2" {
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = var.private_subnet_cidr2_block
  map_public_ip_on_launch = false
  tags = {
    Name = var.private_subnet2_name
  }
}

resource "aws_internet_gateway" "internet_gateway" {
  vpc_id = aws_vpc.vpc.id
}

resource "aws_route_table" "public_route_table" {
  vpc_id = aws_vpc.vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.internet_gateway.id
  }
}

resource "aws_route_table_association" "public_subnet_association" {
  subnet_id      = aws_subnet.public_subnet_1.id
  route_table_id = aws_route_table.public_route_table.id
}

resource "aws_route_table_association" "public_subnet_association_2" {
  subnet_id      = aws_subnet.public_subnet_2.id
  route_table_id = aws_route_table.public_route_table.id
}

resource "aws_route_table_association" "private_subnet_association_1" {
  subnet_id      = aws_subnet.private_subnet1.id
  route_table_id = aws_route_table.public_route_table.id
}

resource "aws_route_table_association" "private_subnet_association_2" {
  subnet_id      = aws_subnet.private_subnet2.id
  route_table_id = aws_route_table.public_route_table.id
}

# resource "aws_subnet" "private_subnet" {
#   vpc_id                  = aws_vpc.vpc.id
#   cidr_block              = var.private_subnet_cidr_block
#   map_public_ip_on_launch = false
#   tags = {
#     Name = "terraform-private-subnet"
#   }
# }



#   tags = {
#     Name = "terraform-public-route-table"
#   }
# }

# resource "aws_route_table_association" "public_subnet_association" {
#   subnet_id      = aws_subnet.public_subnet.id
#   route_table_id = aws_route_table.public_route_table.id
# }

# # resource "aws_route_table_association" "private_subnet_association" {
# #   subnet_id      = aws_subnet.private_subnet.id
# #   route_table_id = aws_route_table.private_route_table.id
# # }

# #Security Groups
# resource "aws_security_group" "security_group" {
#   name_prefix = var.name_prefix
#   vpc_id      = aws_vpc.vpc.id

#   dynamic "ingress" {
#     for_each = [22, 80, 443, 8080]
#     iterator = port
#     content {
#       description = "terraform dynamic loop for ports"
#       from_port   = port.value
#       to_port     = port.value
#       protocol    = "tcp"
#       //security_group_id = aws_security_group.security_group.id
#       cidr_blocks = ["0.0.0.0/0"]
#     }
#   }
#   egress {
#     from_port   = 0
#     to_port     = 0
#     protocol    = "-1"
#     cidr_blocks = ["0.0.0.0/0"]
#   }
#   tags = {
#     Name = "terafform-module-test"
#   }
# }





