module "legacyGS" {
  source                    = "./vpc"
  vpc_cidr_block            = var.cidr_block
  vpc_name                  = "VPC-LegacyGS-Pre01"
  public_subnet1_cidr_block = "10.0.2.0/24"
  public_subnet1_name       = "cb-pre01-public01"

  #availability_zone         = "us-east-1f"
  public_subnet2_name       = "cb-pre01-public02"
  public_subnet2_cidr_block = "10.0.3.0/24"

  private_subnet_cidr1_block = "10.0.0.0/24"
  private_subnet1_name       = "cb-pre01-private01"

  private_subnet_cidr2_block = "10.0.1.0/24"
  private_subnet2_name       = "cb-pre01-private02"

  name = "default"

}