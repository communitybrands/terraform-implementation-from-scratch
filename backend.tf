terraform {
  backend "s3" {
    bucket = "givesmart-tf-statefiles"
    key    = "terraform/terraform.tfstate"
    region = "us-east-1"
  }
}